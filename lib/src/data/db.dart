import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:pethome_app/src/data/model.dart';

class DB {
  static Future<Database> _connectDB() async {
    return openDatabase(
      join(await getDatabasesPath(), 'pethome.db'),
      onCreate: (db, version) {
        db.execute(
          'CREATE TABLE pet(id INTEGER PRIMARY KEY, name TEXT, gender TEXT, race TEXT, age TEXT, description TEXT, img TEXT)',
        );
      },
      version: 1,
    );
  }

  static Future insert(PetLocal pet) async {
    Database database = await _connectDB();
    return database.insert("pet", pet.toMap());
  }

  static Future delete(PetLocal pet) async {
    Database database = await _connectDB();
    return database.delete(
      "pet",
      where: "id = ?",
      whereArgs: [pet.id],
    );
  }

  static Future update(PetLocal pet) async {
    Database database = await _connectDB();
    return database.update(
      "pet",
      pet.toMap(),
      where: "id = ?",
      whereArgs: [pet.id],
    );
  }

  static Future getAll() async {
    Database database = await _connectDB();
    final List<Map<String, dynamic>> pets = await database.query('pet');
    return pets.map((pet) => PetLocal.fromMap(pet)).toList();
  }
}
