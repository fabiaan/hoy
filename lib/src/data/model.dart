class PetLocal {
  PetLocal({
    this.id,
    this.name,
    this.gender,
    this.race,
    this.age,
    this.description,
    this.img,
  });

  int? id;
  String? name;
  String? gender;
  String? race;
  String? age;
  String? description;
  String? img;

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "gender": gender,
        "race": race,
        "age": age,
        "description": description,
        "img": img,
      };

  factory PetLocal.fromMap(Map<String, dynamic> pet) => PetLocal(
        id: pet["id"],
        name: pet["name"],
        gender: pet["gender"],
        race: pet["race"],
        age: pet["age"],
        description: pet["description"],
        img: pet["img"],
      );
}
