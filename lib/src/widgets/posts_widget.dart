import 'package:flutter/material.dart';
import 'package:pethome_app/src/data/db.dart';
import 'package:pethome_app/src/data/model.dart';
import 'package:pethome_app/src/models/publication_model.dart';
import 'package:pethome_app/src/providers/publication_provider.dart';
import 'package:pethome_app/src/providers/user_provider.dart';
import 'package:pethome_app/src/services/publication_service.dart';
import 'package:provider/provider.dart';

class PostsWidget extends StatefulWidget {
  PostsWidget({Key? key}) : super(key: key);

  @override
  State<PostsWidget> createState() => _PostsWidgetState();
}

class _PostsWidgetState extends State<PostsWidget> {
  PetLocal petLocal = PetLocal();

  List<PetLocal> petsLocal = [];

  @override
  void initState() {
    loadPetsLocale();
    super.initState();
  }

  loadPetsLocale() async {
    final pets = await DB.getAll();
    if (pets.length > 0) {
      setState(() {
        petsLocal = pets;
      });
    }
    setState(() {
      petsLocal = pets;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          _header(),
          // _body(),
          _localPublications(),
        ],
      ),
    );
  }

  _header() {
    return Container(
      padding: const EdgeInsets.all(30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: const <Widget>[
              Text(
                'Administra',
                style: TextStyle(
                  fontSize: 25,
                  color: Colors.black,
                  letterSpacing: -0.5,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
          const Text(
            'Tus mascotas en adopción',
            style: TextStyle(
              fontSize: 18,
              color: Colors.black54,
              letterSpacing: -0.5,
            ),
          ),
        ],
      ),
    );
  }

  _body() {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // _PublicationItem(),
          _localPublications(),
        ],
      ),
    );
  }

  _localPublications() {
    final publicationProvider = Provider.of<PublicationProvider>(context);
    final userProvider = Provider.of<UserProvider>(context);
    String token = userProvider.token;

    return FutureBuilder(
      future: publicationProvider.getPublicationsByUserId(token),
      builder: (
        BuildContext context,
        AsyncSnapshot<List<PublicationModel>> snapshot,
      ) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: CircularProgressIndicator.adaptive(),
          );
        }
        final publications = snapshot.data;
        return Expanded(
          child: ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: publications?.length,
            itemBuilder: (context, item) {
              return _publicationItemLocal(publications![item]);
            },
          ),
        );
      },
    );

    // return ListView.builder(
    //   scrollDirection: Axis.vertical,
    //   shrinkWrap: true,
    //   itemCount: petsLocal.length,
    //   itemBuilder: (context, item) {
    //     return _publicationItemLocal(petsLocal[item]);
    //   },
    // );
  }

  _publicationItemLocal(PublicationModel petLocal) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        color: petLocal.state == true
            ? Color.fromARGB(200, 227, 242, 253)
            : Colors.green.shade50,
      ),
      margin: EdgeInsets.only(bottom: 20, left: 20, right: 20),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                padding: const EdgeInsets.all(10),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.network(
                    petLocal.pet?.img ?? '',
                    width: 150.0,
                    height: 150.0,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(
                height: 150.0,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Container(
                          child: Text(
                            petLocal.pet?.name ?? '',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        const SizedBox(width: 5),
                        Container(
                          child: Icon(
                            petLocal.pet?.gender == 'Macho'
                                ? Icons.male_rounded
                                : Icons.female_rounded,
                            color: petLocal.pet?.gender == 'Macho'
                                ? Colors.blue
                                : Colors.pink,
                            size: 20,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Container(
                          child: Text(
                            'Raza: ',
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.black54,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            petLocal.pet?.race ?? '',
                            style:
                                TextStyle(fontSize: 16, color: Colors.black54),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Container(
                          child: Text(
                            'Edad: ',
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.black54,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            int.parse(petLocal.pet?.age ?? '0') > 1
                                ? '${petLocal.pet?.age} años'
                                : '1 año',
                            style:
                                TextStyle(fontSize: 16, color: Colors.black54),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Container(
                          child: Text(
                            'Lugar: ',
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.black54,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            petLocal.user?.location ?? '',
                            style:
                                TextStyle(fontSize: 16, color: Colors.black54),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Container(
                          child: Text(
                            'Estado: ',
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.black54,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            petLocal.state == true ? 'Disponible' : 'Adoptada',
                            style:
                                TextStyle(fontSize: 16, color: Colors.black54),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
          Container(
            padding: EdgeInsets.only(bottom: 10.0, left: 10.0, right: 10.0),
            child: Row(
              children: [
                Expanded(
                  flex: 5,
                  child: TextButton(
                    onPressed: () {
                      Navigator.of(context)
                          .pushNamed('edit', arguments: petLocal);
                    },
                    child: Text(
                      'Editar',
                      style: TextStyle(color: Colors.blue),
                    ),
                    style: TextButton.styleFrom(
                      tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      backgroundColor: Colors.white,
                    ),
                  ),
                ),
                SizedBox(width: 10.0),
                Expanded(
                  flex: 5,
                  child: TextButton(
                    onPressed: () {
                      _marcarComoAdoptada(petLocal);
                    },
                    child: Text(
                      'Adoptada',
                      style: TextStyle(color: Colors.blue),
                    ),
                    style: TextButton.styleFrom(
                      tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      backgroundColor: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _messageLoading() {
    AlertDialog alert = AlertDialog(
      title: Text('Mascota adoptada'),
      content: Row(
        children: [
          CircularProgressIndicator(),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text("Celebremos..."),
          ),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void _marcarComoAdoptada(PublicationModel petLocal) async {
    _messageLoading();
    final publicationProvider =
        Provider.of<PublicationProvider>(context, listen: false);
    final userProvider = Provider.of<UserProvider>(context, listen: false);
    final userId = userProvider.token;
    // DB.delete(petLocal);

    Map<String, dynamic> data = {
      'state': false,
    };

    final response =
        await PublicationService().updatePublication(petLocal.id, data);
    final status = response["status"];

    if (status != 500 && status != 400) {
      await publicationProvider.refreshPublications(userId);
      await publicationProvider.refreshPublicationsByUserId(userId);

      Navigator.pop(context);
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Mascota actualizada correctamente'),
          backgroundColor: Colors.green,
        ),
      );
      await Future.delayed(const Duration(seconds: 2));
      Navigator.pushNamed(context, 'main');
    } else {
      Navigator.pop(context);
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Error al actualizar la mascota'),
          backgroundColor: Colors.red,
        ),
      );
    }
  }
}
