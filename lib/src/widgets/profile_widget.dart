import 'package:flutter/material.dart';
import 'package:pethome_app/src/models/user_model.dart';
import 'package:pethome_app/src/pages/login_page.dart';
import 'package:pethome_app/src/providers/publication_provider.dart';
import 'package:pethome_app/src/providers/user_provider.dart';
import 'package:provider/provider.dart';

class ProfileWidget extends StatefulWidget {
  ProfileWidget({Key? key}) : super(key: key);

  @override
  State<ProfileWidget> createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends State<ProfileWidget> {
  final TextStyle _textStyleTitle = const TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.bold,
    color: Colors.blue,
    letterSpacing: -0.5,
  );

  final TextStyle _textStyleData = const TextStyle(
    fontSize: 18,
    color: Colors.black54,
    letterSpacing: -0.5,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          _header(),
          _body(),
        ],
      ),
    );
  }

  _header() {
    return Container(
      padding: const EdgeInsets.all(30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: const <Widget>[
              Text(
                'Configura',
                style: TextStyle(
                  fontSize: 25,
                  color: Colors.black,
                  letterSpacing: -0.5,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
          const Text(
            'Los datos de tu cuenta',
            style: TextStyle(
              fontSize: 18,
              color: Colors.black54,
              letterSpacing: -0.5,
            ),
          ),
        ],
      ),
    );
  }

  _body() {
    final userProvider = Provider.of<UserProvider>(context);
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 30),
        // child: _createProfileUser(userData),
        child: FutureBuilder(
          future: userProvider.getDataUser(),
          builder: (
            BuildContext context,
            AsyncSnapshot<UserModel> snapshot,
          ) {
            if (snapshot.hasData) {
              final user = snapshot.data;
              return _createProfileUser(user);
            } else {
              return const Center(child: CircularProgressIndicator());
            }
          },
        ),
      ),
    );
  }

  _buildDivider() {
    return Column(
      children: [
        const SizedBox(height: 10),
        Divider(color: Colors.black.withOpacity(0.3)),
        const SizedBox(height: 10),
      ],
    );
  }

  _createProfileUser(UserModel? user) {
    print("AAAAA::::::::::" + user!.name.toString());

    final letter = user.name!.substring(0, 1);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: Colors.blue.shade50,
            shape: BoxShape.circle,
          ),
          padding: const EdgeInsets.all(40),
          child: Text(
            letter,
            style: TextStyle(
              fontSize: 30,
              color: Colors.blue,
            ),
          ),
        ),
        const SizedBox(height: 20),
        Container(
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Nombre',
                style: _textStyleTitle,
              ),
              const SizedBox(height: 5),
              Padding(
                padding: EdgeInsets.only(left: 10),
                child: Text(
                  user.name.toString(),
                  style: _textStyleData,
                ),
              ),
              _buildDivider(),
              Text(
                'Correo',
                style: _textStyleTitle,
              ),
              const SizedBox(height: 5),
              Padding(
                padding: EdgeInsets.only(left: 10),
                child: Text(
                  user.email.toString(),
                  style: _textStyleData,
                ),
              ),
              _buildDivider(),
              Text(
                'Teléfono',
                style: _textStyleTitle,
              ),
              const SizedBox(height: 5),
              Padding(
                padding: EdgeInsets.only(left: 10),
                child: Text(
                  user.phone.toString(),
                  style: _textStyleData,
                ),
              ),
              _buildDivider(),
              Text(
                'Dirección',
                style: _textStyleTitle,
              ),
              const SizedBox(height: 5),
              Padding(
                padding: EdgeInsets.only(left: 10),
                child: Text(
                  user.location.toString(),
                  style: _textStyleData,
                ),
              ),
              const SizedBox(height: 30),
              Row(
                children: [
                  Expanded(
                    child: Container(
                      width: double.infinity,
                      child: ElevatedButton(
                        onPressed: _logout,
                        child: Text(
                          'Cerrar sesión',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                          ),
                        ),
                        style: ElevatedButton.styleFrom(
                          fixedSize: const Size(double.infinity, 60),
                          primary: Colors.blue,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }

  _logout() {
    final publicationProvider =
        Provider.of<PublicationProvider>(context, listen: false);
    final userProvider = Provider.of<UserProvider>(context, listen: false);

    Navigator.pushAndRemoveUntil<void>(
      context,
      MaterialPageRoute<void>(
          builder: (BuildContext context) => const LoginPage()),
      ModalRoute.withName('login'),
    );

    publicationProvider.deletePublications();
    userProvider.deleteUser();
  }
}
