import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pethome_app/src/data/db.dart';
import 'package:pethome_app/src/data/model.dart';
import 'package:pethome_app/src/models/pet_model.dart';
import 'package:pethome_app/src/models/publication_model.dart';
import 'package:pethome_app/src/providers/image_provider.dart';
import 'package:pethome_app/src/providers/publication_provider.dart';
import 'package:pethome_app/src/providers/user_provider.dart';
import 'package:pethome_app/src/services/pet_service.dart';
import 'package:pethome_app/src/services/publication_service.dart';
import 'package:pethome_app/src/widgets/button_loader_widget.dart';
import 'package:provider/provider.dart';

class AddPetWidget extends StatefulWidget {
  AddPetWidget({Key? key}) : super(key: key);

  @override
  State<AddPetWidget> createState() => _AddPetWidgetState();
}

class _AddPetWidgetState extends State<AddPetWidget> {
  String? dropdownValue;
  var items = ['Macho', 'Hembra'];
  final _imageProvider = ImageProviderBB();
  XFile? foto;

  final _formKey = GlobalKey<FormState>();
  // PetLocal petLocal = PetLocal();
  PetModel petLocal = PetModel();

  bool _isLoading = false;

  OutlineInputBorder errorOutlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10),
    borderSide: const BorderSide(
      color: Colors.red,
      width: 1.3,
    ),
  );

  OutlineInputBorder enableOutlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10),
    borderSide: const BorderSide(
      color: Colors.black54,
      width: 1.3,
    ),
  );

  OutlineInputBorder focusedOutlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10),
    borderSide: const BorderSide(
      color: Colors.blue,
      width: 1.3,
    ),
  );

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          _header(),
          _body(),
        ],
      ),
    );
  }

  _header() {
    return Container(
      padding: const EdgeInsets.all(30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: const <Widget>[
              Text(
                'Publica',
                style: TextStyle(
                  fontSize: 25,
                  color: Colors.black,
                  letterSpacing: -0.5,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
          const Text(
            'Una mascota en adopción',
            style: TextStyle(
              fontSize: 18,
              color: Colors.black54,
              letterSpacing: -0.5,
            ),
          ),
        ],
      ),
    );
  }

  _body() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 30),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _showPhoto(),
            const SizedBox(height: 20),
            _buttonAddImage(),
            const SizedBox(height: 20),
            _name(),
            const SizedBox(height: 20),
            _gender(),
            const SizedBox(height: 20),
            _age(),
            const SizedBox(height: 20),
            _race(),
            const SizedBox(height: 20),
            _description(),
            const SizedBox(height: 20),
            _buttonPublication(),
            const SizedBox(height: 30),
          ],
        ),
      ),
    );
  }

  _name() {
    return Container(
      child: TextFormField(
        onSaved: (value) => petLocal.name = value,
        validator: (value) {
          return _validateWithRegex(
            r"^.{3,}$",
            value,
            "El campo debe tener al menos 3 caracteres",
          );
        },
        keyboardType: TextInputType.text,
        style: const TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          labelText: 'Nombre',
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
        ),
      ),
    );
  }

  _gender() {
    return Container(
      // decoration: BoxDecoration(
      //   border: Border.all(
      //     color: Colors.black54,
      //     width: 1.3,
      //   ),
      //   borderRadius: BorderRadius.all(Radius.circular(10.0)),
      // ),
      // child: Padding(
      //   padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
      //   child: DropdownButton(
      //     value: dropdownValue,
      //     isExpanded: true,
      //     items: items.map((String items) {
      //       return DropdownMenuItem(
      //         value: items,
      //         child: Text(items),
      //       );
      //     }).toList(),
      //     onChanged: (String? newValue) {
      //       setState(() {
      //         dropdownValue = newValue!;
      //       });
      //     },
      //     underline: Container(),
      //     style: TextStyle(
      //       fontSize: 20,
      //       color: Colors.black54,
      //     ),
      //     elevation: 1,
      //     hint: Text('Género'),
      //   ),
      // ),
      child: DropdownButtonFormField(
        onSaved: (String? value) => petLocal.gender = value,
        validator: (value) {
          return _validateWithRegex(
            r"^.{3,}$",
            value,
            "El campo debe tener un género seleccionado",
          );
        },
        items: items.map((String items) {
          return DropdownMenuItem(
            value: items,
            child: Text(items),
          );
        }).toList(),
        onChanged: (String? newValue) {
          setState(() {
            dropdownValue = newValue!;
          });
        },
        decoration: InputDecoration(
          labelText: 'Género',
          labelStyle: TextStyle(
            fontSize: 20,
          ),
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
        ),
        style: TextStyle(
          fontSize: 20,
          color: Colors.black,
        ),
        // hint: Text('Género'),
        elevation: 1,
      ),
    );
  }

  _age() {
    return Container(
      child: TextFormField(
        onSaved: (value) => petLocal.age = value,
        validator: (value) {
          return _validateWithRegex(
            r"^.{1,}$",
            value,
            "El campo debe tener al menos 1 caracter",
          );
        },
        keyboardType: TextInputType.number,
        style: const TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          labelText: 'Edad',
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
        ),
      ),
    );
  }

  _race() {
    return Container(
      child: TextFormField(
        onSaved: (value) => petLocal.race = value,
        validator: (value) {
          return _validateWithRegex(
            r"^.{5,}$",
            value,
            "El campo debe tener al menos 5 caracteres",
          );
        },
        keyboardType: TextInputType.text,
        style: const TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          labelText: 'Raza',
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
        ),
      ),
    );
  }

  _validateWithRegex(regex, value, message) {
    if (value.toString().isEmpty) return 'Este campo es requerido';
    if (!RegExp(regex).hasMatch(value ?? '')) return message;
    return null;
  }

  _description() {
    return Container(
      child: TextFormField(
        onSaved: (value) => petLocal.description = value,
        validator: (value) {
          return _validateWithRegex(
            r"^.{25,}$",
            value,
            "El campo debe tener al menos 25 caracteres",
          );
        },
        maxLines: 3,
        keyboardType: TextInputType.multiline,
        style: const TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          alignLabelWithHint: true,
          labelText: 'Descripción',
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
        ),
      ),
    );
  }

  _buttonAddImage() {
    return Container(
      width: double.infinity,
      child: ElevatedButton(
        onPressed: () => _selectPhoto(),
        child: Text(
          'Añadir imagen',
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
          ),
        ),
        style: ElevatedButton.styleFrom(
          fixedSize: const Size(double.infinity, 60),
          primary: Colors.blue,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }

  _buttonPublication() {
    return Container(
      width: double.infinity,
      child: ElevatedButton(
        onPressed: () => _uploadPet(),
        child: Text(
          'Publicar',
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
          ),
        ),
        style: ElevatedButton.styleFrom(
          fixedSize: const Size(double.infinity, 60),
          primary: Colors.blue,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }

  _showPhoto() {
    if (foto != null) {
      return Container(
        width: double.infinity,
        height: 270,
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          child: Image.file(
            File(foto!.path),
            fit: BoxFit.cover,
          ),
        ),
      );
    } else {
      return Container(
        width: double.infinity,
        height: 270,
        child: Icon(
          FeatherIcons.image,
          size: 28,
          color: Colors.blue,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.blue.shade50,
        ),
      );
    }
  }

  _selectPhoto() async {
    foto = await ImagePicker().pickImage(source: ImageSource.gallery);

    if (foto != null) {
      setState(() {
        foto = foto;
      });
    }
  }

  _messageLoading() {
    AlertDialog alert = AlertDialog(
      title: Text('Creando mascota'),
      content: Row(
        children: [
          CircularProgressIndicator(),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text("Validando datos..."),
          ),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  _messageSelectPhoto() {
    AlertDialog alert = AlertDialog(
      title: Text('Error al publicar'),
      content: Text('Selecciona una foto'),
      actions: [
        TextButton(
          onPressed: _salir,
          child: Text('Aceptar'),
        )
      ],
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  _salir() {
    Navigator.pop(context);
    return;
  }

  _uploadPet() async {
    final publicationProvider =
        Provider.of<PublicationProvider>(context, listen: false);
    final userProvider = Provider.of<UserProvider>(context, listen: false);
    final userId = userProvider.token;

    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      if (foto != null) {
        setState(() => _isLoading = true);
        _messageLoading();

        final bytes = File(foto!.path).readAsBytesSync();
        String base64Image = base64Encode(bytes);
        final responseImg = await _imageProvider.upload(base64Image);
        if (responseImg['status'] == 200) {
          String url = responseImg['data']['url'];
          petLocal.img = url;
          // DB.insert(petLocal);

          final response = await PetService().createPet(petLocal);
          final status = response["status"];

          if (status != 500 && status != 400) {
            final petSaveId = response["_id"];

            Map<String, dynamic> publication = {
              "pet": petSaveId,
              "user": userProvider.token,
            };

            print("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA::" +
                publication.toString());

            final responsePublication =
                await PublicationService().createPublication(publication);

            if (responsePublication["status"] != 500 &&
                responsePublication["status"] != 400) {
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(
                  content: Text('Mascota publicada'),
                  backgroundColor: Colors.green,
                ),
              );

              await publicationProvider.refreshPublications(userId);
              await publicationProvider.refreshPublicationsByUserId(userId);

              Navigator.pop(context);
              await Future.delayed(const Duration(seconds: 2));
              Navigator.pushReplacementNamed(context, 'main');

              setState(() => _isLoading = false);
            }
          }
        } else {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text("Ocurrió un error al subir la imagen"),
              backgroundColor: Colors.red,
            ),
          );
        }
      } else {
        _messageSelectPhoto();
      }
    }
  }
}
