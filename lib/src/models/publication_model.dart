import 'dart:convert';

PublicationModel publicationModelFromJson(String str) =>
    PublicationModel.fromJson(json.decode(str));

String publicationModelToJson(PublicationModel data) =>
    json.encode(data.toJson());

class PublicationModel {
  PublicationModel({
    this.id,
    this.state,
    this.pet,
    this.user,
  });

  String? id;
  bool? state;
  Pet? pet;
  User? user;

  factory PublicationModel.fromJson(Map<String, dynamic> json) =>
      PublicationModel(
        id: json["_id"],
        state: json["state"],
        pet: Pet.fromJson(json["pet"]),
        user: User.fromJson(json["user"]),
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "state": state,
        "pet": pet?.toJson(),
        "user": user?.toJson(),
      };
}

class Pet {
  Pet({
    this.id,
    this.name,
    this.gender,
    this.race,
    this.age,
    this.description,
    this.img,
  });

  String? id;
  String? name;
  String? gender;
  String? race;
  String? age;
  String? description;
  String? img;

  factory Pet.fromJson(Map<String, dynamic> json) => Pet(
        id: json["_id"],
        name: json["name"],
        gender: json["gender"],
        race: json["race"],
        age: json["age"],
        description: json["description"],
        img: json["img"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "name": name,
        "gender": gender,
        "race": race,
        "age": age,
        "description": description,
        "img": img,
      };
}

class User {
  User({
    this.id,
    this.name,
    this.phone,
    this.location,
    this.email,
  });

  String? id;
  String? name;
  String? phone;
  String? location;
  String? email;

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["_id"],
        name: json["name"],
        phone: json["phone"],
        location: json["location"],
        email: json["email"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "name": name,
        "phone": phone,
        "location": location,
        "email": email,
      };
}
