import 'dart:convert';

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));

String userModelToJson(UserModel data) => json.encode(data.toJson());

String userModelToJsonLogin(UserModel data) => json.encode(data.toJsonLogin());

class UserModel {
  UserModel({
    this.id,
    this.name,
    this.phone,
    this.location,
    this.email,
    this.password,
  });

  String? id;
  String? name;
  String? phone;
  String? location;
  String? email;
  String? password;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        id: json["_id"],
        name: json["name"],
        phone: json["phone"],
        location: json["location"],
        email: json["email"],
        password: json["password"],
      );

  factory UserModel.fromJsonProfile(Map<String, dynamic> json) => UserModel(
        id: json["_id"],
        name: json["name"],
        phone: json["phone"],
        location: json["location"],
        email: json["email"],
        password: "",
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "name": name,
        "phone": phone,
        "location": location,
        "email": email,
        "password": password,
      };

  Map<String, dynamic> toJsonLogin() => {
        "email": email,
        "password": password,
      };
}
