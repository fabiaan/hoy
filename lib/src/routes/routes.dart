import 'package:flutter/material.dart';
import 'package:pethome_app/src/pages/edit_pet_page.dart';
import 'package:pethome_app/src/pages/introduction_page.dart';
import 'package:pethome_app/src/pages/login_page.dart';
import 'package:pethome_app/src/pages/main_page.dart';
import 'package:pethome_app/src/pages/pet_page.dart';
import 'package:pethome_app/src/pages/register_page.dart';

Map<String, WidgetBuilder> getApplicationRoutes() {
  return <String, WidgetBuilder>{
    'login': (BuildContext context) => LoginPage(),
    'register': (BuildContext context) => RegisterPage(),
    'intro': (BuildContext context) => const IntroductionPage(),
    'main': (BuildContext context) => MainPage(),
    'pet': (BuildContext context) => const PetPage(),
    'edit': (BuildContext context) => EditPetPage(),
  };
}
