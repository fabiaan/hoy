import 'package:flutter/material.dart';

Container styleBannerPage = Container(
  width: 50,
  height: 50,
  alignment: Alignment.center,
  child: const Icon(
    Icons.arrow_back_ios_rounded,
    color: Colors.blue,
  ),
  decoration: BoxDecoration(
    borderRadius: BorderRadius.circular(20),
    color: Colors.blue.shade50,
  ),
);

const TextStyle styleTitlePage = TextStyle(
  fontSize: 25,
  color: Colors.black,
  letterSpacing: -0.5,
  fontWeight: FontWeight.bold,
);

const TextStyle styleRacePage = TextStyle(
  fontSize: 18,
  color: Colors.black54,
  letterSpacing: -0.5,
);

BoxDecoration styleBoxsPage = BoxDecoration(
  borderRadius: BorderRadius.circular(20),
  color: Colors.blue.shade50,
);

const TextStyle styleTitleBoxPage = TextStyle(
  color: Colors.blue,
  fontSize: 20,
  fontWeight: FontWeight.bold,
);

const TextStyle styleDescriBoxPage = TextStyle(
  fontSize: 16,
  color: Colors.black87,
);

const TextStyle styleInfoPage = TextStyle(
  fontSize: 20,
  color: Colors.black,
  fontWeight: FontWeight.bold,
);

const TextStyle styleTitleContactPage = TextStyle(
  color: Colors.blue,
  fontSize: 16,
  fontWeight: FontWeight.bold,
);
