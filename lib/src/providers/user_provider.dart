import 'package:flutter/material.dart';
import 'package:pethome_app/src/models/user_model.dart';
import 'package:pethome_app/src/services/user_service.dart';

class UserProvider extends ChangeNotifier {
  String _token = '';
  UserModel user = UserModel();

  String get token {
    return _token;
  }

  set token(String value) {
    _token = value;
    notifyListeners();
  }

  Future<UserModel> getDataUser() async {
    if (user.id != null) {
      return user;
    }

    var service = UserService();
    user = await service.getUserInfo(_token);
    return user;
  }

  Future<UserModel> saveDataUser(id) async {
    var service = UserService();
    user = await service.getUserInfo(id);
    notifyListeners();
    return user;
  }

  Future<UserModel> refreshUser(id) async {
    var service = UserService();
    user = await service.getUserInfo(id);
    notifyListeners();
    return user;
  }

  deleteUser() {
    user = UserModel();
    _token = '';
    return;
  }
}
