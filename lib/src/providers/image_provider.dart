import 'dart:convert';

import 'package:http/http.dart' as http;

class ImageProviderBB {
  final String _baseUrl = 'api.imgbb.com';

  upload(img) async {
    const String _pathUrl = '1/upload';
    Map<String, String> queryParameters = {
      'key': '4561a8bc41dbb805915506566fe932c9',
    };
    final url = Uri.https(_baseUrl, _pathUrl, queryParameters);

    final response = await http.post(
      url,
      body: {'image': img},
    );

    final decodeData = json.decode(response.body);
    return decodeData;
  }
}
