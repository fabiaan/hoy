import 'package:flutter/material.dart';
import 'package:pethome_app/src/models/publication_model.dart';
import 'package:pethome_app/src/services/publication_service.dart';

class PublicationProvider extends ChangeNotifier {
  bool publicacionesUserCallAPI = false;
  bool publicacionesCallAPI = false;
  List<PublicationModel> publicaciones = [];
  List<PublicationModel> publicacionesUser = [];

  Future<List<PublicationModel>> getPublications(userId) async {
    if (publicacionesCallAPI == false) {
      publicacionesCallAPI = true;
      var service = PublicationService();
      publicaciones = await service.getPublications(userId);

      notifyListeners();
      return publicaciones;
    } else {
      return publicaciones;
    }
  }

  Future<List<PublicationModel>> getPublicationsByUserId(id) async {
    if (publicacionesUserCallAPI == false) {
      publicacionesUserCallAPI = true;
      var service = PublicationService();
      publicacionesUser = await service.getPublicationsByUserId(id);

      notifyListeners();
      return publicacionesUser;
    } else {
      return publicacionesUser;
    }
  }

  Future<List<PublicationModel>> refreshPublications(userId) async {
    var service = PublicationService();
    publicaciones = await service.getPublications(userId);
    notifyListeners();
    return publicaciones;
  }

  Future<List<PublicationModel>> refreshPublicationsByUserId(id) async {
    var service = PublicationService();
    publicacionesUser = await service.getPublicationsByUserId(id);
    notifyListeners();
    return publicacionesUser;
  }

  deletePublications() {
    publicaciones.clear();
    publicacionesUser.clear();
    publicacionesUserCallAPI = false;
    publicacionesCallAPI = false;
    return;
  }
}
