import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pethome_app/src/models/user_model.dart';

class UserService {
  final String _baseUrl = 'api-adoption-pets.onrender.com';
  final Map<String, String> headers = {
    'Content-Type': 'application/json; charset=UTF-8',
  };

  register(UserModel user) async {
    const String _pathUrl = 'v1/api/auth/signup';
    final url = Uri.https(_baseUrl, _pathUrl);

    final response = await http.post(
      url,
      headers: headers,
      body: userModelToJson(user),
    );
    final decodeData = json.decode(response.body);
    return decodeData;
  }

  login(UserModel user) async {
    const String _pathUrl = 'v1/api/auth/signin';
    final url = Uri.https(_baseUrl, _pathUrl);

    final response = await http.post(
      url,
      headers: headers,
      body: userModelToJsonLogin(user),
    );
    final decodeData = json.decode(response.body);
    return decodeData;
  }

  getUserInfo(id) async {
    String _pathUrl = 'v1/api/user/$id';
    final url = Uri.https(_baseUrl, _pathUrl);

    final response = await http.get(url, headers: headers);
    final decodeData = json.decode(response.body);

    UserModel user = UserModel.fromJsonProfile(decodeData);
    return user;
  }
}
