import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:pethome_app/src/models/publication_model.dart';

class PublicationService {
  final String _baseUrl = 'api-adoption-pets.onrender.com';
  final Map<String, String> headers = {
    'Content-Type': 'application/json; charset=UTF-8',
  };

  Future<List<PublicationModel>> getPublications(userId) async {
    String _pathUrl = 'v1/api/publication/v2/$userId';
    final url = Uri.https(_baseUrl, _pathUrl);

    final response = await http.get(url, headers: headers);
    final decodeData = json.decode(response.body);
    final List<PublicationModel> publications = [];

    if (decodeData == null) return [];

    decodeData.forEach((element) {
      final publicationTemp = PublicationModel.fromJson(element);
      publications.add(publicationTemp);
    });

    return publications;
  }

  Future<List<PublicationModel>> getPublicationsByUserId(id) async {
    String _pathUrl = 'v1/api/publication/$id';
    final url = Uri.https(_baseUrl, _pathUrl);

    final response = await http.get(url, headers: headers);
    final decodeData = json.decode(response.body);
    final List<PublicationModel> publications = [];

    if (decodeData == null) return [];

    decodeData.forEach((element) {
      final publicationTemp = PublicationModel.fromJson(element);
      publications.add(publicationTemp);
    });

    return publications;
  }

  createPublication(publication) async {
    const String _pathUrl = 'v1/api/publication';
    final url = Uri.https(_baseUrl, _pathUrl);

    final response = await http.post(
      url,
      headers: headers,
      body: json.encode(publication),
    );
    final decodeData = json.decode(response.body);
    return decodeData;
  }

  updatePublication(id, publication) async {
    String _pathUrl = 'v1/api/publication/$id';
    final url = Uri.https(_baseUrl, _pathUrl);

    final response = await http.put(
      url,
      headers: headers,
      body: json.encode(publication),
    );
    final decodeData = json.decode(response.body);
    return decodeData;
  }
}
