import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pethome_app/src/models/pet_model.dart';

class PetService {
  final String _baseUrl = 'api-adoption-pets.onrender.com';
  final Map<String, String> headers = {
    'Content-Type': 'application/json; charset=UTF-8',
  };

  createPet(PetModel pet) async {
    const String _pathUrl = 'v1/api/pet';
    final url = Uri.https(_baseUrl, _pathUrl);

    final response = await http.post(
      url,
      headers: headers,
      body: petModelToJsonCreatePet(pet),
    );
    final decodeData = json.decode(response.body);
    return decodeData;
  }

  updatePet(PetModel pet) async {
    String _pathUrl = 'v1/api/pet/${pet.id}';
    final url = Uri.https(_baseUrl, _pathUrl);

    final response = await http.put(
      url,
      headers: headers,
      body: petModelToJson(pet),
    );
    final decodeData = json.decode(response.body);
    return decodeData;
  }
}
