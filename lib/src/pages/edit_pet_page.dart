import 'package:flutter/material.dart';
import 'package:pethome_app/src/data/db.dart';
import 'package:pethome_app/src/data/model.dart';
import 'package:pethome_app/src/models/pet_model.dart';
import 'package:pethome_app/src/models/publication_model.dart';
import 'package:pethome_app/src/providers/publication_provider.dart';
import 'package:pethome_app/src/providers/user_provider.dart';
import 'package:pethome_app/src/services/pet_service.dart';
import 'package:pethome_app/src/widgets/button_loader_widget.dart';
import 'package:provider/provider.dart';

class EditPetPage extends StatefulWidget {
  EditPetPage({Key? key}) : super(key: key);

  @override
  State<EditPetPage> createState() => _EditPetPageState();
}

class _EditPetPageState extends State<EditPetPage> {
  String? dropdownValue;
  var items = ['Macho', 'Hembra'];

  OutlineInputBorder errorOutlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10),
    borderSide: const BorderSide(
      color: Colors.red,
      width: 1.3,
    ),
  );

  OutlineInputBorder enableOutlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10),
    borderSide: const BorderSide(
      color: Colors.black54,
      width: 1.3,
    ),
  );

  OutlineInputBorder focusedOutlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10),
    borderSide: const BorderSide(
      color: Colors.blue,
      width: 1.3,
    ),
  );

  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    // final petLocal = ModalRoute.of(context)!.settings.arguments as PetLocal;
    final petLocal =
        ModalRoute.of(context)!.settings.arguments as PublicationModel;

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(30.0),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      width: 50,
                      height: 50,
                      alignment: Alignment.center,
                      child: const Icon(
                        Icons.arrow_back_ios_rounded,
                        color: Colors.blue,
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.blue.shade50,
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  Container(
                    width: width,
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      child: Image.network(
                        petLocal.pet?.img ?? '',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  const SizedBox(height: 20),
                  _name(petLocal),
                  const SizedBox(height: 20),
                  _gender(petLocal),
                  const SizedBox(height: 20),
                  _age(petLocal),
                  const SizedBox(height: 20),
                  _race(petLocal),
                  const SizedBox(height: 20),
                  _description(petLocal),
                  const SizedBox(height: 20),
                  _buttonPublication(petLocal),
                  const SizedBox(height: 30),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _name(petLocal) {
    return Container(
      child: TextFormField(
        onSaved: (value) => petLocal.pet.name = value,
        validator: (value) {
          return _validateWithRegex(
            r"^.{3,}$",
            value,
            "El campo debe tener al menos 3 caracteres",
          );
        },
        initialValue: petLocal.pet.name,
        keyboardType: TextInputType.text,
        style: const TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          labelText: 'Nombre',
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
        ),
      ),
    );
  }

  _gender(petLocal) {
    return Container(
      child: DropdownButtonFormField(
        onSaved: (String? value) => petLocal.pet.gender = value,
        validator: (value) {
          return _validateWithRegex(
            r"^.{3,}$",
            value,
            "El campo debe tener un género seleccionado",
          );
        },
        value: items[0] == petLocal.pet.gender ? items[0] : items[1],
        items: items.map((String items) {
          return DropdownMenuItem(
            value: items,
            child: Text(items),
          );
        }).toList(),
        onChanged: (String? newValue) {
          setState(() {
            dropdownValue = newValue!;
          });
        },
        decoration: InputDecoration(
          labelText: 'Género',
          labelStyle: TextStyle(
            fontSize: 20,
          ),
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
        ),
        style: TextStyle(
          fontSize: 20,
          color: Colors.black,
        ),
        // hint: Text('Género'),
        elevation: 1,
      ),
    );
  }

  _age(petLocal) {
    return Container(
      child: TextFormField(
        onSaved: (value) => petLocal.pet.age = value,
        validator: (value) {
          return _validateWithRegex(
            r"^.{1,}$",
            value,
            "El campo debe tener al menos 1 caracter",
          );
        },
        initialValue: petLocal.pet.age,
        keyboardType: TextInputType.text,
        style: const TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          labelText: 'Edad',
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
        ),
      ),
    );
  }

  _race(petLocal) {
    return Container(
      child: TextFormField(
        onSaved: (value) => petLocal.pet.race = value,
        validator: (value) {
          return _validateWithRegex(
            r"^.{5,}$",
            value,
            "El campo debe tener al menos 5 caracteres",
          );
        },
        initialValue: petLocal.pet.race,
        keyboardType: TextInputType.text,
        style: const TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          labelText: 'Raza',
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
        ),
      ),
    );
  }

  _description(petLocal) {
    return Container(
      child: TextFormField(
        onSaved: (value) => petLocal.pet.description = value,
        validator: (value) {
          return _validateWithRegex(
            r"^.{25,}$",
            value,
            "El campo debe tener al menos 25 caracteres",
          );
        },
        initialValue: petLocal.pet.description,
        maxLines: 3,
        keyboardType: TextInputType.multiline,
        style: const TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          alignLabelWithHint: true,
          labelText: 'Descripción',
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
        ),
      ),
    );
  }

  _validateWithRegex(regex, value, message) {
    if (value.isEmpty) return 'Este campo es requerido';
    if (!RegExp(regex).hasMatch(value)) return message;
    return null;
  }

  _buttonPublication(petLocal) {
    return Container(
      width: double.infinity,
      child: ElevatedButton(
        onPressed: () {
          _updateLocalPet(petLocal);
        },
        child: Text(
          'Actualizar',
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
          ),
        ),
        style: ElevatedButton.styleFrom(
          fixedSize: const Size(double.infinity, 60),
          primary: Colors.blue,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }

  _messageLoading() {
    AlertDialog alert = AlertDialog(
      title: Text('Editando mascota'),
      content: Row(
        children: [
          CircularProgressIndicator(),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text("Validando datos..."),
          ),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void _updateLocalPet(petLocal) async {
    final publicationProvider =
        Provider.of<PublicationProvider>(context, listen: false);
    final userProvider = Provider.of<UserProvider>(context, listen: false);
    final userId = userProvider.token;

    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      setState(() => _isLoading = true);
      _messageLoading();

      PetModel petEdit = PetModel(
        id: petLocal.pet.id,
        name: petLocal.pet.name,
        age: petLocal.pet.age,
        description: petLocal.pet.description,
        gender: petLocal.pet.gender,
        img: petLocal.pet.img,
        race: petLocal.pet.race,
      );

      final response = await PetService().updatePet(petEdit);
      final status = response['status'];

      if (status != 500 && status != 400) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('Mascota editada correctamente'),
            backgroundColor: Colors.green,
          ),
        );

        await publicationProvider.refreshPublications(userId);
        await publicationProvider.refreshPublicationsByUserId(userId);

        Navigator.pop(context);
        await Future.delayed(const Duration(seconds: 2));
        Navigator.pushReplacementNamed(context, 'main');

        setState(() => _isLoading = false);
      } else {
        Navigator.pop(context);
        setState(() => _isLoading = false);
        final message = response["message"];
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(message),
            backgroundColor: Colors.red,
          ),
        );
      }
    }
  }
}
