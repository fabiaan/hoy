import 'package:flutter/material.dart';
import 'package:pethome_app/src/styles/pet_page_style.dart';
import 'package:pethome_app/src/data/model.dart';
import 'package:pethome_app/src/models/publication_model.dart';

class PetPage extends StatelessWidget {
  const PetPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    final publication =
        ModalRoute.of(context)!.settings.arguments as PublicationModel;
    final activo = publication.state == true ? 'Disponible' : 'Adoptado';

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(30.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: styleBannerPage,
                ),
                SizedBox(height: 20),
                Container(
                  width: width,
                  height: width,
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    child: Image.network(
                      publication.pet?.img ?? '',
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      publication.pet?.name ?? '',
                      style: styleTitlePage,
                    ),
                    Container(
                      child: Icon(
                        publication.pet?.gender == 'Macho'
                            ? Icons.male_rounded
                            : Icons.female_rounded,
                        color: Colors.black38,
                        size: 30,
                      ),
                    ),
                  ],
                ),
                Text(
                  publication.pet?.race ?? '',
                  style: styleRacePage,
                ),
                SizedBox(height: 20),
                GridView.count(
                  shrinkWrap: true,
                  crossAxisSpacing: 10,
                  crossAxisCount: 3,
                  children: [
                    Container(
                      decoration: styleBoxsPage,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'Edad',
                            style: styleTitleBoxPage,
                          ),
                          Text(
                            int.parse(publication.pet?.age ?? '0') > 1
                                ? '${publication.pet?.age} años'
                                : '1 año',
                            style: styleDescriBoxPage,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      decoration: styleBoxsPage,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'Sexo',
                            style: styleTitleBoxPage,
                          ),
                          Text(
                            publication.pet?.gender ?? '',
                            style: styleDescriBoxPage,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      decoration: styleBoxsPage,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'Estado',
                            style: styleTitleBoxPage,
                          ),
                          Text(
                            activo,
                            style: styleDescriBoxPage,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Text(
                  'Descripción',
                  style: styleInfoPage,
                ),
                SizedBox(height: 10),
                Text(
                  publication.pet?.description ?? '',
                  style: styleDescriBoxPage,
                ),
                SizedBox(height: 10),
                Text(
                  'Contacto',
                  style: styleInfoPage,
                ),
                SizedBox(height: 10),
                Container(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Container(
                              child: Text(
                                'Nombre:',
                                style: styleTitleContactPage,
                              ),
                            ),
                            const SizedBox(width: 5),
                            Container(
                              child: Text(
                                publication.user?.name ?? '',
                                style: styleDescriBoxPage,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 10),
                        Row(
                          children: [
                            Container(
                              child: Text(
                                'Télefono:',
                                style: styleTitleContactPage,
                              ),
                            ),
                            const SizedBox(width: 5),
                            Container(
                              child: Text(
                                publication.user?.phone ?? '',
                                style: styleDescriBoxPage,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 10),
                        Row(
                          children: [
                            Container(
                              child: Text(
                                'Ubicación:',
                                style: styleTitleContactPage,
                              ),
                            ),
                            const SizedBox(width: 5),
                            Container(
                              child: Text(
                                publication.user?.location ?? '',
                                style: styleDescriBoxPage,
                              ),
                            ),
                          ],
                        ),
                      ]),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
