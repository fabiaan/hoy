import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:pethome_app/src/models/user_model.dart';
import 'package:pethome_app/src/providers/user_provider.dart';
import 'package:pethome_app/src/services/user_service.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();

  final userService = UserService();
  final userProvider = UserProvider();
  UserModel user = UserModel();
  bool _isLoading = false;
  bool _isVisible = false;

  _viewPassword() {
    setState(() {
      _isVisible = !_isVisible;
    });
  }

  OutlineInputBorder errorOutlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10),
    borderSide: const BorderSide(
      color: Colors.red,
      width: 1.3,
    ),
  );

  OutlineInputBorder enableOutlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10),
    borderSide: const BorderSide(
      color: Colors.black54,
      width: 1.3,
    ),
  );

  OutlineInputBorder focusedOutlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10),
    borderSide: const BorderSide(
      color: Colors.blue,
      width: 1.3,
    ),
  );

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Column(children: <Widget>[
            _container(_isVisible, _viewPassword),
          ]),
        ),
      ),
    );
  }

  _container(_isVisible, _viewPassword) {
    return Container(
      padding: const EdgeInsets.all(40),
      child: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            _leftButton(),
            const SizedBox(height: 30),
            _subtitle(),
            const SizedBox(height: 30),
            _emailInput(),
            const SizedBox(height: 20),
            _passwordInput(_isVisible, _viewPassword),
            const SizedBox(height: 20),
            _loginButton(),
            const SizedBox(height: 20),
            _footer(),
            const SizedBox(height: 20),
          ],
        ),
      ),
    );
  }

  _logo() {
    return const Text(
      'Pethouse',
      style: TextStyle(
        fontSize: 30,
        fontWeight: FontWeight.w900,
        color: Colors.blue,
        letterSpacing: -0.5,
      ),
    );
  }

  _emailInput() {
    return Container(
      child: TextFormField(
        onSaved: (value) => user.email = value,
        validator: (value) {
          return _validateWithRegex(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+",
            value,
            "El campo debe tener un correo válido",
          );
        },
        keyboardType: TextInputType.emailAddress,
        style: const TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          labelText: 'Correo',
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
        ),
      ),
    );
  }

  _passwordInput(_isVisible, _viewPassword) {
    return Container(
      child: TextFormField(
        onSaved: (value) => user.password = value,
        validator: (value) {
          return _validateWithRegex(
            r"^.{8,}$",
            value,
            "El campo debe tener al menos 8 caracteres",
          );
        },
        obscureText: !_isVisible,
        style: const TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          labelText: 'Contraseña',
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
          suffixIcon: IconButton(
            onPressed: () {
              _viewPassword();
            },
            icon: _isVisible
                ? const Icon(FeatherIcons.eye)
                : const Icon(FeatherIcons.eyeOff),
          ),
        ),
      ),
    );
  }

  _loginButton() {
    return Container(
      child: ElevatedButton(
        onPressed: _submit,
        child: const Text(
          'Iniciar sesión',
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
        style: ElevatedButton.styleFrom(
          fixedSize: const Size(1000, 60),
          primary: Colors.blue,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }

  _subtitle() {
    return Container(
      alignment: Alignment.centerLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: const <Widget>[
          Text(
            'Bienvenido de nuevo',
            style: TextStyle(
              fontSize: 25,
              color: Colors.black,
              letterSpacing: -0.5,
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            'Inicia sesión para continuar',
            style: TextStyle(
              fontSize: 18,
              color: Colors.black54,
              letterSpacing: -0.5,
            ),
          ),
        ],
      ),
    );
  }

  _footer() {
    return Container(
      alignment: Alignment.center,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          const Text(
            '¿No tienes cuenta?',
            style: TextStyle(
              fontSize: 18,
              color: Colors.black54,
              letterSpacing: -0.5,
            ),
          ),
          TextButton(
            onPressed: () {
              Navigator.pushNamed(context, 'register');
            },
            child: const Text(
              'Regístrate',
              style: TextStyle(
                fontSize: 18,
                color: Colors.blue,
                letterSpacing: -0.5,
              ),
            ),
          ),
        ],
      ),
    );
  }

  _leftButton() {
    return Container(
      alignment: Alignment.centerLeft,
      child: Ink(
        child: IconButton(
          splashRadius: 25,
          icon: const Icon(
            Icons.arrow_back_ios_rounded,
            color: Colors.blue,
          ),
          onPressed: () {
            Navigator.of(context).pushNamed('intro');
          },
        ),
        decoration: ShapeDecoration(
          color: Colors.blue.shade50,
          shape: const CircleBorder(),
        ),
      ),
    );
  }

  _validateWithRegex(regex, value, message) {
    if (value.isEmpty) return 'Este campo es requerido';
    if (!RegExp(regex).hasMatch(value)) return message;
    return null;
  }

  _messageLoading(BuildContext context) {
    AlertDialog alert = AlertDialog(
      title: Text('Iniciando sesión'),
      content: Row(
        children: [
          CircularProgressIndicator(),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text("Validando datos..."),
          ),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void _submit() async {
    final userProvider = Provider.of<UserProvider>(context, listen: false);
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      setState(() => _isLoading = true);
      _messageLoading(context);

      final response = await userService.login(user);
      final status = response["status"];
      print(response["user"]);

      if (status != 500 && status != 400) {
        UserModel user = UserModel.fromJsonProfile(response["user"]);
        userProvider.token = user.id.toString();
        await userProvider.saveDataUser(user.id);

        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('¡Has iniciado sesión correctamente!'),
            backgroundColor: Colors.green,
          ),
        );

        Navigator.pop(context);
        await Future.delayed(const Duration(seconds: 2));
        Navigator.pushReplacementNamed(context, 'main');

        setState(() => _isLoading = false);
      } else {
        Navigator.pop(context);
        setState(() => _isLoading = false);
        final message = response["message"];
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(message),
            backgroundColor: Colors.red,
          ),
        );
      }
    }
  }
}
